import requests, json, random
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_picture(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": f"{city} {state}"}
    url = "https://api.pexels.com/v1/search"
    res = requests.get(url, headers=headers, params=params)
    the_json = res.json()
    random_number = random.randint(0, 5)
    picture_dict = {
        "picture_url": the_json["photos"][random_number]["src"]["original"],
    }
    return picture_dict


def get_geo(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct?"
    params = {
        "appid": OPEN_WEATHER_API_KEY,
        "q": f"{city}, {state}, USA",
    }
    res = requests.get(geo_url, params=params)
    the_json = res.json()
    lat = the_json[0]["lat"]
    lon = the_json[0]["lon"]
    return lat, lon


def get_weather_data(city, state):
    lat, lon = get_geo(city, state)
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "appid": OPEN_WEATHER_API_KEY,
        "lat": lat,
        "lon": lon,
        "units": "imperial",
    }
    wea = requests.get(weather_url, params=weather_params)
    weather_json = wea.json()
    weather_dict = {
        "temp": weather_json["main"]["temp"],
        "description": weather_json["weather"][0]["description"],
    }
    return weather_dict
