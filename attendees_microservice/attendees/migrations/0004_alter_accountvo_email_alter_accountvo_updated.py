# Generated by Django 4.0.3 on 2022-07-14 22:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0003_alter_attendee_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountvo',
            name='email',
            field=models.EmailField(max_length=254, unique=True),
        ),
        migrations.AlterField(
            model_name='accountvo',
            name='updated',
            field=models.DateField(auto_now_add=True, null=True),
        ),
    ]
